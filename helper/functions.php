<?php

use Cocur\Slugify\Slugify;
use duncan3dc\Laravel\BladeInstance;

use Illuminate\Config\Repository;


define('DOCUMENT_ROOT', $_SERVER['DOCUMENT_ROOT']);




function spin($string)
{
  $spintax = new Spinner();
  return $spintax->process($string);
}

/**
 *  LIMIT WORDS COUNT
 */

function limit_the_words($text, $mark)
{
  if (is_numeric($mark)) {
    $text = explode(" ", $text);
    $text = implode(" ", array_splice($text, 0, $mark));
  } else {
    $text = explode($mark, $text)[0];
  }
  return $text;
}


function Minify_Html($Html)
{
  $Search = array(
    '/(\n|^)(\x20+|\t)/',
    '/(\n|^)\/\/(.*?)(\n|$)/',
    '/\n/',
    //'/\<\!--.*?-->/',
    '/(\x20+|\t)/', # Delete multispace (Without \n)
    '/\>\s+\</', # strip whitespaces between tags
    '/(\"|\')\s+\>/', # strip whitespaces between quotation ("') and end tags
    '/=\s+(\"|\')/'
  ); # strip whitespaces between = "'

  $Replace = array(
    "\n",
    "\n",
    " ",
    //"",
    " ",
    "><",
    "$1>",
    "=$1"
  );

  $Html = preg_replace($Search, $Replace, $Html);
  //$Html = str_replace('more_vyant','<!--more-->',$Html);

  return $Html;
}

function agcPdfCleanKw($string)
{

  $array = ['download', 'pdf', 'doc', 'docx'];
  foreach ($array as $key => $val) {
    $string = str_replace($val, '', strtolower($string));
  }

  return trim(ucwords($string));
}


function makeTitleFromSlug($slug)
{
  $title = str_replace('.json', '', $slug);
  $title = str_replace('-', ' ', $title);
  $title = ucwords($title);
  return $title;
}


function getTimeStamp()
{
  return  date('Y-m-d H:i:s');
}
function export($template, $data = [], $echo = true)
{
  $blade = new BladeInstance($_SERVER['DOCUMENT_ROOT'] . 'views',  $_SERVER['DOCUMENT_ROOT'] . 'cache');
  // // $blade->addPath(__DIR__ . '/edit');
  // // $blade->addPath(__DIR__ . '/pages');
  // // $blade->addPath(__DIR__ . '/ads');
  // // $blade->addPath(__DIR__ . '/notice');

  return $blade->render($template, $data);
}

function view($template, $data = [], $echo = true)
{
  $blade = new BladeInstance($_SERVER['DOCUMENT_ROOT'] . 'views',  $_SERVER['DOCUMENT_ROOT'] . 'cache');
  // // $blade->addPath(__DIR__ . '/edit');
  // // $blade->addPath(__DIR__ . '/pages');
  // // $blade->addPath(__DIR__ . '/ads');
  // // $blade->addPath(__DIR__ . '/notice');

  if (!$echo) {
    return $blade->render($template, $data);
  }

  $html =  $blade->render($template, $data);
  echo sanitize_output($html);
}




function templateList()
{
  $dirList = [];
  $dir = scandir(DOCUMENT_ROOT . 'views/template/');
  array_shift($dir);
  array_shift($dir);
  foreach ($dir as $file) {
    array_push($dirList, str_replace('.blade.php', '', $file));
  }

  return $dirList;
}

function getTemplate($name)
{
  return $name;
}


function uuid($data = null)
{
  // Generate 16 bytes (128 bits) of random data or use the data passed into the function.
  $data = $data ?? random_bytes(16);
  assert(strlen($data) == 16);

  // Set version to 0100
  $data[6] = chr(ord($data[6]) & 0x0f | 0x40);
  // Set bits 6-7 to 10
  $data[8] = chr(ord($data[8]) & 0x3f | 0x80);

  // Output the 36 character UUID.
  return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
}


function makeSLug($string, $separator = '-')
{
  return (new Slugify())->slugify($string, $separator);
}

function rrmdir($dir)
{
  if (is_dir($dir)) {
    $objects = scandir($dir);
    foreach ($objects as $object) {
      if ($object != "." && $object != "..") {
        if (is_dir($dir . DIRECTORY_SEPARATOR . $object) && !is_link($dir . "/" . $object))
          rrmdir($dir . DIRECTORY_SEPARATOR . $object);
        else
          unlink($dir . DIRECTORY_SEPARATOR . $object);
      }
    }
    rmdir($dir);
  }
}


/*
 *	Config like Laravel
 */
function config($configFiles)
{
  $dir = DOCUMENT_ROOT;

  if (php_sapi_name() === 'cli') {
    $dir = getcwd();
  }
  if (strpos($configFiles, '.') !== false) {
    $getData    = explode('.', $configFiles);
    $configFile = $getData[0];
    $params      = implode('.', array_slice($getData, 1));
  } else {
    $configFile  = $configFiles;
  }

  if (!file_exists($dir . '/config/' . $configFile . '.php')) return;

  $config = new Repository(require $dir . '/config/' . $configFile . '.php');

  if (strpos($configFiles, '.') !== false) {
    return $config->get($params);
  }

  return $config->all();
}


function arrayToObject($array)
{
  return json_decode(json_encode($array));
}


function db()
{

  if (Flight::has('db')) {
    return Flight::get('db');
  }
  $db_name = config('database.dbName');
  $db_user = config('database.dbUser');
  $db_db_password = config('database.dbPass');
  $pdo = new PDO('mysql:dbname=' . $db_name, $db_user, $db_db_password);
  $instance = new \Envms\FluentPDO\Query($pdo);
  return $instance;
}



function isLogin()
{

  if (!isset($_SESSION['user'])) {
    // Flight::redirect('/auth/login');
  }
}


function makePassword($string)
{
  return password_hash($string, PASSWORD_BCRYPT);
}



function createAdmin()
{

  $exist = db()->from('users')->where('level', 'superuser')->limit(1);

  if (!count($exist) > 0) {
    $user  = [
      'id' => uuid(),
      'email' => 'gustirama01@gmail.com',
      'password' => makePassword('123'),
      'status' => 'active',
      'level' => 'superuser'
    ];
    $insert = db()->insertInto('users')->values($user)->execute();
    if ($insert) {
      Flight::redirect('/auth/login');
    }
  }
}
