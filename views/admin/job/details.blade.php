@extends('admin.layout')
@section('title', 'Job ' . $data->name . ' ' . $data->id . ' Details')

@section('content')

  <table class="jobdetails">
    <tbody>
      <tr>
        <td>Prepend</td>
        <td>{{ $data->prepend }}</td>
      </tr>
      <tr>
        <td>Append</td>
        <td>{{ $data->append }}</td>
      </tr>
      <tr>
        <td>Template</td>
        <td>{{ $data->template }}</td>
      </tr>
      <tr>
        <td>Total</td>
        <td>{{ count(explode("\r\n", $data->keywords)) }}</td>
      </tr>
      <tr>
        <td>Ready To Export</td>
        <form action="" method="post">
          <input type="hidden" name="id" value="{{ $data->id }}">
          <td>
            @switch($data->ready)
              @case(1)
                <input type="hidden" name="action" value="scrape">
                <input type="submit" value="Scrape">
              @break
              @case(0)
                <span class="loading">Loading</span> -
                <input type="hidden" name="action" value="rescrape">
                <input type="submit" value="re-scrape">

              @break

              @case(2)
                <input type="hidden" name="action" value="export">
                <input type="submit" value="Export">
              @break
              @default

            @endswitch
            @isset($data->ready)


            @else

            @endisset


          </td>

        </form>
      </tr>
      <tr>
        <td>AGC PDF</td>
        <td>
          <textarea rows="10" name="keywords" wrap="soft"
            style="border: none; width: 100%; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">{{ $data->keywords }}</textarea>
          <ul>

          </ul>

        </td>
      </tr>

    </tbody>
  </table>




@endsection
