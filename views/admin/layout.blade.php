<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="/assets/css/style.css">
  <title>@yield('title')</title>
  @stack('css')
  @stack('header')
</head>

<body>
  <header>
    @includeIf('admin.header')
  </header>
  <section class="content">
    @yield('content')
  </section>
  <footer>

  </footer>
  <script src="/assets/js/script.js"></script>
  @stack('js')
  @stack('footer')

</body>

</html>
