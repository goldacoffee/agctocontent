@php
if (isset($data->sentences)) {
    $sentences = collect($data->sentences);
}
$title = makeTitleFromSlug($title);
$cleanTitle = agcPdfCleanKw($title);

@endphp


<h1>{{ $title }}</h1>
<figure class="article-thumb">
  <img src="{{ $data->images[0]->thumb }}" class="article-thumb" alt="Thumbnail for {{ $data->images[0]->title }}"
    loading="lazy">
</figure>


<p style="text-align: justify;">
  {!! $sentences->shuffle()->take(5)->implode('<br/>') !!}
</p>

<!--more-->
<p>
  {{ spin('{Are you|Do you}') }} {{ spin('{looking for|finding}') }} something related to
  <strong> {{ $cleanTitle }}</strong> ? {{ spin('{Well|Congrats}') }} you come to the right places. In this post i
  will share you about &nbsp;<strong>{{ $cleanTitle }}</strong> , not only the
  {{ spin('{Images|Pictures}') }} but also the PDF file related to <strong>{{ $cleanTitle }}</strong><br>

  @if (!empty($data->pdfs))
    We already found about &nbsp;<strong>{{ count($data->pdfs) }}</strong> related document for &nbsp;
    <strong>{{ $cleanTitle }}</strong>. And we also have about &nbsp;
    <strong>{{ count($data->images) }}</strong>&nbsp;{{ spin('{Images|Pictures}') }} about
    <strong>{{ $cleanTitle }}</strong>
  @endif


</p>

<div class="pdf-download-area">
  @if (!empty($data->pdfs))
    <table class="pdf-table-area">
      <thead>
        <tr>
          <td>#</td>
          <td>PDF Title</td>
        </tr>
      </thead>

      <tbody>
        @php
          shuffle($data->pdfs);
        @endphp
        @foreach ($data->pdfs as $key => $pdf)
          @if ($key < 20)
            <tr>
              <td>
                {{ $key + 1 }}
              </td>
              <td>
                <a target="_blank" href="{{ $pdf->pdflink }}"
                  title="Download PDF file for {{ $pdf->title }} ">{{ $pdf->title }}</a>
              </td>
            </tr>

          @endif

        @endforeach

      </tbody>
    </table>
  @endif



</div>

@php
shuffle($data->images);
$images = collect($data->images)->chunk(9);
@endphp

@foreach ($images as $keyx => $chunk)
  <h3 class="gallery-title-h3">Gallery Image {{ $keyx + 1 }} {{ $cleanTitle }}</h3>
  <div class="gallery-container">
    <div class="gallery">
      @foreach ($chunk as $key => $item)
        <div class="gallery-item">
          <a href="{{ $item->image }}" target="_blank" title="{{ $item->title }}">
            <img class="gallery-image" src="{{ $item->thumb }}" alt="{{ $item->title }}">
          </a>
        </div>
      @endforeach
    </div>
  </div>

@endforeach
