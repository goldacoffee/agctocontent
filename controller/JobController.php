<?php

use Cocur\Slugify\Slugify;

class JobController
{
  public static function getJobList()
  {
    $jobList = Flight::get('db')->from('jobs')->orderBy('modified_at DESC');
    return $jobList;
  }


  public static function getGenerateList()
  {
    $jobDir = scandir(DOCUMENT_ROOT . 'job/generate/');
    array_shift($jobDir);
    array_shift($jobDir);

    array_reverse($jobDir);



    $dirJob = [];
    foreach ($jobDir as $key => $name) {
      $name = trim($name);
      if ($name !== '.gitkeep' && $name !== "") {
        $dataLoc = DOCUMENT_ROOT . 'job/generate/' . $name;
        $array = json_decode(file_get_contents($dataLoc));
        array_push($dirJob, $array);
      }
    }



    return $dirJob;
  }

  public static function insertJob($data)
  {
    Flight::get('db')->insertInto('jobs', $data)->execute();

    /**
     * below code is unused since admin migrated to using database for greater use
     */
    // $fileName = $data['id'] . '.json';
    // $json = json_encode($data);
    // file_put_contents(DOCUMENT_ROOT . 'job/queue/' . $fileName, $json);
  }

  public static function deleteJob($slug)
  {

    db()->deleteFrom('jobs')->where('id', $slug)->execute();

    $fileName = $slug . '.json';

    try {
      // unlink(DOCUMENT_ROOT . 'job/log/' . $fileName);
      rrmdir(DOCUMENT_ROOT . 'job/queue/' . $slug);
      rrmdir(DOCUMENT_ROOT . 'job/data/' . $slug);
      if (file_exists(DOCUMENT_ROOT . 'job/queue/' . $fileName)) {

        unlink(DOCUMENT_ROOT . 'job/queue/' . $fileName);
      }
      if (file_exists(DOCUMENT_ROOT . 'job/generate/' . $fileName)) {

        unlink(DOCUMENT_ROOT . 'job/generate/' . $fileName);
      }
    } catch (\Throwable $th) {
      throw $th;
    }
  }


  public static function goGenerateSingle($request)
  {
    // print_r($request->keyword);
    $id = uuid();
    $initialData = [
      'id' => $id,
      'status' => 0,
      'append' => $request->append,
      'prepend' => $request->prepend,
      'template' => $request->template,
      'is_pdf' => $request->is_pdf,
      'keyword' => $request->keyword,
    ];
    $filePath = DOCUMENT_ROOT . 'job/generate/' . $id . '.json';
    file_put_contents($filePath, json_encode($initialData));

    if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
      $cmd = "php " . DOCUMENT_ROOT . "processGenerate.php " . $id . "";
      pclose(popen("start /B " . $cmd, "w"));
    } else {
      $cmd = "php " . DOCUMENT_ROOT . "processGenerate.php " . $id . "";
      $proc = new BackgroundProcess($cmd);
    }

    Flight::redirect('/single-generate/');
  }

  public static function doSingleGenerate($id)
  {
    $dataPath = DOCUMENT_ROOT . 'job/generate/' . $id . '.json';
    $dataArray = json_decode(file_get_contents($dataPath));
    $kw = $dataArray->keyword;
    $dataBing = BingGrabber::bing_execute($kw, $dataArray->is_pdf);
    $dataArray->data = $dataBing;
    $dataArray->status = 1;

    file_put_contents($dataPath, json_encode($dataArray));
  }
  public static function doSingleJob($id)
  {
    $dirName = DOCUMENT_ROOT . 'job/data/' . $id;

    if (!is_dir($dirName)) {
      mkdir($dirName);
    }
    echo "Get :  keywords from ID: $id \n";
    $dataLoc = DOCUMENT_ROOT . 'job/queue/' . $id . '.json';

    $data = db()->from('jobs')->where('id', $id)->fetch();


    $dataArray = arrayToObject($data);
    $dataArray->ready = 0;
    $mainLogFileName =   DOCUMENT_ROOT . 'job/queue/' . $id . '.json';
    self::writeMainLog(0, $dataArray);
    $jobName = $dataArray->id;
    $jobTemplate = $dataArray->template;
    $keywords = explode("\r\n", $dataArray->keywords);
    foreach ($keywords as $key => $kw) {
      $kw = trim($kw);
      if ($kw === '') {
        echo "Empty Keyword Found, skipping... \n";
        break;
      }
      try {
        $fileName = makeSLug($kw) . '.json';
        if ($key === 1) {
          if (is_file($dirName . '/logs.txt')) {
            file_put_contents($dirName . '/logs.txt', "");
          }
        }

        if (!is_file($dirName . '/' . $fileName)) {
          echo "Grabbing Images For Keyword : $kw \n";
          try {


            $dataBing = BingGrabber::bing_execute($kw, $dataArray->is_pdf);
            file_put_contents($dirName . '/' . $fileName, json_encode($dataBing));

            $log = $key + 1 . '- ' . $kw . " done";
            echo "Saving Data... \n";
            self::writeLog($dirName . '/logs.txt', $log);
            sleep(rand(2, 5));
          } catch (\Throwable $th) {
            echo "Not Enought Data KW :  $kw ,skipping... \n";
            continue;
          }
        } else {
          $log = $key + 1 . '- ' . $kw . " Skipping";
          echo "Data found, skipping... \n";
          self::writeLog($dirName . '/logs.txt', $log);
        }
      } catch (\Throwable $th) {
        throw $th;
      }
    }


    $dataArray->ready = 2;
    echo "Job Finished, you may export... \n\n";
    /**
     * dont forget disable this line on development mode
     */
    self::sendJobDoneNotification($id, $dataArray->name);
    self::writeMainLog(2, $dataArray);
  }

  public static function writeLog($fileName, $data)
  {

    file_put_contents($fileName, $data . PHP_EOL, FILE_APPEND | LOCK_EX);
  }
  public static function writeMainLog($ready, $data)
  {
    $set = [
      'ready' => 2,
      'modified_at' => getTimeStamp()
    ];

    db()->update('jobs')->set($set)->where('id', $data->id)->execute();
  }


  public static function sendJobDoneNotification($jobId, $jobName, $to = 'rendrian@gustiarma.com')
  {
    $cmd = "php " . DOCUMENT_ROOT . "sendMail.php " . $jobId . " " . $to . ' "' . $jobName . '"';


    if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
      // $cmd = "php " . DOCUMENT_ROOT . "sendMail.php " . $jobId . " " . $to . " " . '""';
      pclose(popen("start /B " . $cmd, "w"));
    } else {

      $proc = new BackgroundProcess($cmd);
    }
  }

  public static function getJobLog($slug)
  {

    $data = db()->from('jobs')->where('id', $slug)->fetch();

    return arrayToObject($data);
    // $filePath = DOCUMENT_ROOT . 'job/queue/' . $slug . '.json';
    // $data = file_get_contents($filePath);


    // return json_decode($data);
  }
  public static function sample()
  {
    self::doExportJob('b5115943-d12f-4caa-89f3-41e0bf33c0c5');
  }
  public static function doExportJob($slug)
  {
    // $filename = DOCUMENT_ROOT . 'job/queue/' . $slug . '.json';
    // $config = json_decode(file_get_contents($filename));
    // $config->ready = 3;
    // self::writeMainLog(3, $config);
    $config = db()->from('jobs')->where('id', $slug)->fetch();
    $config = arrayToObject($config);
   
    $template = $config->template;
    $folderPath = DOCUMENT_ROOT . 'job/data/' . $slug;

    $scanDir = scandir($folderPath);
    $finalArray = [];
    foreach ($scanDir as $file) {
      if (strpos($file, '.json') !== false) {
        $array = json_decode(file_get_contents($folderPath . '/' . $file));
        $html = export('template.' . $template, [
          'title' => $file,
          'data' => $array
        ], false);
        $forContent = [
          'title' => $file,
          'content' => $html,
          'config' => $config
        ];

        array_push($finalArray, $forContent);
      }
    }


    // header("Content-Type:text/xml");


    $xml = export('exports.blogger', ['data' => $finalArray]);
    $config->ready = 4;
    $unique = makeSLug($config->name) . date('Y-m-d-H-i-s');
    $config->xml_file = $unique . '.xml';
    self::writeMainLog(4, $config);
    file_put_contents(DOCUMENT_ROOT . 'job/finished/' . $unique . '.xml', $xml);
  }

  public static function runningProcess()
  {
    $proc = new BackgroundProcess();
    print_r($proc->showAllProcess());
  }
}
